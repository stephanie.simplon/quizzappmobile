import { Component, OnInit } from "@angular/core";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    private end: boolean = false;
    private num: number = 0;
    private results: boolean[] = [];
    private questions: { question: string, answer: boolean }[] =
    [
        {question: 'You can lead a cow down stairs but not up stairs.', answer: true},
        {question: 'Approximately one quarter of human bones are in the feet.', answer: true},
        {question: 'A slug\'s blood is green.', answer: true},
        {question: 'Buzz Aldrin\'s mother\'s maiden name was \"Moon\".', answer: true},
        {question: 'It is illegal to pee in the Ocean in Portugal.', answer: true},
        {question: 'No piece of square dry paper can be folded in half more than 7 times.', answer: false},
        {question: 'In London, UK, if you happen to die in the House of Parliament, you are technically entitled to a state funeral, because the building is considered too sacred a place.', answer: true},
        {question: 'The loudest sound produced by any animal is 188 decibels. That animal is the African Elephant.', answer: false},
        {question: 'The total surface area of two human lungs is approximately 70 square metres.', answer: true},
        {question: 'Google was originally called \"Backrub\".', answer: true},
        {question: 'Chocolate affects a dog\'s heart and nervous system; a few ounces are enough to kill a small dog.', answer: true},
        {question: 'In West Virginia, USA, if you accidentally hit an animal with your car, you are free to take it home to eat.', answer: true}
    ]

    constructor() {
    }

    ngOnInit(): void {
        this.num = 0;
        this.results = [];
        this.end = false;
    }

    onButtonTap(numero: number, rep: boolean) {
        if (numero <= this.questions.length-1 && !(this.results.length === this.questions.length)) {
            if (rep === this.questions[numero].answer) {
                this.results.push(true);
            } else this.results.push(false);
        } else if (this.results.length === this.questions.length) {
            this.end = true;
        }
        if (numero < this.questions.length-1) {
            this.num++;
        }
        if (this.end === true) {
            this.num = 0;
            this.results = [];
            this.end = false;
        }
    }

}
